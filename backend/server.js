'use strict';

const express = require('express');
const test = require('./test')
var retour = {};

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello World');
});

app.listen(process.env.BACKEND_PORT, () => {
  console.log(`Running on http://localhost:${process.env.BACKEND_PORT}`);
   retour = test.execute();
});

app.get('/getUsers', (req, res) => {
  res.send(JSON.stringify(retour.user));
});

app.get('/getPosts', (req, res) => {
  res.send(JSON.stringify(retour.post));
});