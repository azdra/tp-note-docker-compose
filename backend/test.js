const Mongoose = require("mongoose");
const {ObjectId} = require('mongoose')

const Execute = async () => {

    let url = "mongodb://mongodb:27017/test";

    try {

        let client = await Mongoose.connect( url, {
            authSource: "admin",
            user: "root",
            pass: "secret",
            useNewUrlParser: true,
            useUnifiedTopology: true
        } );

        const User = client.model('User', { name: String, age: Number, email: String });
        const Post = client.model('Post', { title: String, content: String, user: String });

        const newUser = new User({name: 'John Smith', age: 25, mail: 'jesuistonpere@gmail.com' });
        newUser.save().then((r) => {
            console.log('User save', r)
            const newPost = new Post({tile: 'Article', content: 'Mon contenu', user: r._id.valueOf()})
            newPost.save().then((a) => {
                console.log('Post save', a)
            })
        })

        return {
            'user': await User.find({}),
            'post': await Post.find({})
        }
    } catch ( error ) {
        console.log( error.stack );
        process.exit( 1 );
    }

}

module.exports.execute = Execute
// Connect();

// mongoose.connect(`mongodb://root:secret@mongodb:27017/test`);
// mongoose.createConnection(
//   "mongodb://mongodb:27017/test",
//   {
//     "auth": {
//       "authSource": "admin"
//     },
//     "user": "root",
//     "pass": "secret"
//   }
// )
/*mongoose.createConnection(
  "mongodb://localhost:27017/dbName",
  {
    "auth": {
      "authSource": "admin"
    },
    "user": "admin",
    "pass": "password"
  }
);*/

// const Cat = mongoose.model('Cat', { name: String });
//
// const kitty = new Cat({ name: 'Zildjian' });
// kitty.save().then(() => console.log('meow'));